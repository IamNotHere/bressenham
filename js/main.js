// Canvas var setup
const canvas = document.getElementById("canv");
const ctx = canvas.getContext("2d");

// scale defines the amount fo divion
var scale = 33;
// sizeOffset defines how much smaller than a cell is a pixel
const sizeOffset = 5;
// Additional variables to avoid re-calculating on each frame
var divisionX = canvas.width / (canvas.width / scale);
var divisionY = canvas.height / (canvas.height / scale);
var resX = canvas.width / scale;
var rexY = canvas.width / scale;
// For when scale changes
var forceRenderUpdate = false;

const slider = document.getElementById("scaleSlider");
const scaleDisplay = document.getElementById("scale");

slider.value = scale;
scaleDisplay.innerHTML = "Scale: " + scale;

slider.addEventListener("input", (e) => {
    updateScale(e.target.value);
    scaleDisplay.innerHTML = "Scale: " + e.target.value;
});

// Starting position of the line, default=center
var startpos = {
    x: Math.floor(divisionX / 2),
    y: Math.floor(divisionY / 2)
};

// Cursor position
var cursor = {
    x: -1,
    y: -1,
};

// Stats element
const stats = document.getElementById("stats");

// Variables for FPS counting
const fpsdiv = document.getElementById("fps");
var lastCalledTime;
var fps;

// Variables for render() function
prev_cursor = {
    x: -1,
    y: -1,
};

// INIT
function init() {
    clear();
    render();

    // Set slow interval for FPS display
    setInterval(() => {
        fpsdiv.innerHTML = "FPS: " + Math.floor(fps);
    }, 200);

    updateStats(stats, startpos, cursor);
}

function updateScale(x) {
    scale = x;
    divisionX = canvas.width / (canvas.width / scale);
    divisionY = canvas.height / (canvas.height / scale);
    resX = canvas.width / scale;
    rexY = canvas.width / scale;

    startpos = {
        x: Math.floor(divisionX / 2),
        y: Math.floor(divisionY / 2),
    };

    cursor = {
        x: startpos.x,
        y: startpos.y,
    };

    forceRenderUpdate = true;
}

// Draw a cell grid
function drawGrid(color = "#99aab5", lineWidth = 1) {
    for (let i = 0; i <= divisionX - 0; i++) {
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo((canvas.width / scale) * i, 0);
        ctx.lineTo((canvas.width / scale) * i, canvas.height);
        ctx.stroke();
    }
    for (let i = 0; i <= divisionY - 0; i++) {
        ctx.strokeStyle = color;
        ctx.lineWidth = lineWidth;
        ctx.beginPath();
        ctx.moveTo(0, (canvas.height / scale) * i);
        ctx.lineTo(canvas.width, (canvas.height / scale) * i);
        ctx.stroke();
    }
}

// Clear the canvas and draw grid
function clear() {
    ctx.clearRect(0, 0, canvas.height, canvas.width);
    drawGrid("#99aab5", (1 / scale) * 50);
}

// Draw individual pixels
function drawPixel(x, y, color = "#b7ff00") {
    x = x < 0 ? 0 : x;
    y = y < 0 ? 0 : y;
    ctx.fillStyle = color;
    ctx.fillRect(
        x * resX + sizeOffset / 4,
        y * rexY + sizeOffset / 4,
        resX - sizeOffset / 2,
        rexY - sizeOffset / 2
    );
}

// Draw a line between 2 points
function drawLine(x0, y0, x1, y1) {
    let dx = Math.abs(x1 - x0);
    let dy = Math.abs(y1 - y0);
    let sx = x0 < x1 ? 1 : -1;
    let sy = y0 < y1 ? 1 : -1;
    let err = dx - dy;

    while (true) {
        drawPixel(x0, y0);

        if (x0 == x1 && y0 == y1) break;
        let e2 = 2 * err;
        if (e2 > -dy) {
            err -= dy;
            x0 += sx;
        }
        if (e2 < dx) {
            err += dx;
            y0 += sy;
        }
    }
}

// Get absolute real cursor position on canvas
function getMousePos(canvas, e) {
    var rect = canvas.getBoundingClientRect(),
        scaleX = canvas.width / rect.width,
        scaleY = canvas.height / rect.height;

    return {
        x: Math.floor((e.clientX - rect.left) * scaleX),
        y: Math.floor((e.clientY - rect.top) * scaleY),
    };
}

// Update cursor position on cursor move
canvas.addEventListener("mousemove", (e) => {
    let mousePos = getMousePos(canvas, e);
    cursor.x = Math.ceil(mousePos.x / (canvas.width / divisionX)) - 1;
    cursor.y = Math.ceil(mousePos.y / (canvas.height / divisionY)) - 1;
});

// Move the starting point when clicked
canvas.addEventListener("mousedown", (e) => {
    let mousePos = getMousePos(canvas, e);
    cursor.x = Math.ceil(mousePos.x / (canvas.width / divisionX)) - 1;
    cursor.y = Math.ceil(mousePos.y / (canvas.height / divisionY)) - 1;
    startpos.x = cursor.x;
    startpos.y = cursor.y;
    clear();
    drawPixel(cursor.x, cursor.y, "#0000ff");
});

// Clear canvas if mouse left it
canvas.addEventListener("mouseleave", (e) => {
    cursor = {
        x: startpos.x,
        y: startpos.y,
    };
    clear();
});

// Update displayed stats
function updateStats(element, point1, point2) {
    let p1_desc =
        "Blue position: <br>X:" +
        Number(point1.x + 1) +
        " <br>Y:" +
        Number(point1.y + 1);
    let p2_desc =
        "Red position: <br>X:" +
        Number(point2.x + 1) +
        " <br>Y:" +
        Number(point2.y + 1);

	let custom =
		"Distance: <br>DX:" +
        Number(Math.abs(point1.x - point2.x)) +
        " <br>DY:" +
        Number(Math.abs(point1.y - point2.y)) +
		"<br> Diff:"+Number(Math.abs(point1.x - point2.x)-Math.abs(point1.y - point2.y));
    element.innerHTML = p1_desc + "<br>" + p2_desc + "<br>" + custom;
}

// Render: clear, calc FPS, draw line, draw pixels
function render() {
    requestAnimationFrame(render);

    // FPS counting
    if (!lastCalledTime) {
        lastCalledTime = Date.now();
        fps = 0;
    }
    delta = (Date.now() - lastCalledTime) / 1000;
    lastCalledTime = Date.now();
    fps = 1 / delta;

    updateStats(stats, startpos, cursor);

    // Only draw if mouse position is diffrent
    if (
        prev_cursor.x != cursor.x ||
        prev_cursor.y != cursor.y ||
        forceRenderUpdate == true
    ) {
        clear();
        drawLine(startpos.x, startpos.y, cursor.x, cursor.y);
        drawPixel(cursor.x, cursor.y, "#ff0000");
        drawPixel(startpos.x, startpos.y, "#0000ff");

        prev_cursor.x = cursor.x;
        prev_cursor.y = cursor.y;

        forceRenderUpdate = false;
        // console.log(mousePosPixelX, mousePosPixelY);
    }
}

init();