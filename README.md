# Bressenham

![project icon](/icon.png)

This small project is intended as a demonstration of the Bressenham line drawing algorithm.

Below is the code for the line draw function which draws the line.

`dx` and `dy` are the X and Y distances between the 2 points, `sx` and `sx` define the direction the line will be heading towards from the 1st point.

	function drawLine(x0, y0, x1, y1) {
		let dx = Math.abs(x1 - x0);
		let dy = Math.abs(y1 - y0);
		let sx = x0 < x1 ? 1 : -1;
		let sy = y0 < y1 ? 1 : -1;
		let err = dx - dy;

		while (true) {
			drawPixel(x0, y0);

			if (x0 == x1 && y0 == y1) break;
			let e2 = 2 * err;
			if (e2 > -dy) {
				err -= dy;
				x0 += sx;
			}
			if (e2 < dx) {
				err += dx;
				y0 += sy;
			}
		}
	}